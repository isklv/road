package ru.garyk.roadtowork;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by Sokolov on 21.05.2015.
 */
public class FragmentHelper {

    public static final String LOGIN = "login";
    public static final String USERINFO = "userinfo";

    public static void Replace(FragmentManager fragmentManager, String tag){

        FragmentTransaction frTransaction = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        if(fragment == null) {
            switch (tag) {
                case LOGIN:
                    fragment = LoginFragment.newInstance();
                    break;
                case USERINFO:
                    fragment = UserFragment.newInstance();
                    break;
            }
        }

        frTransaction.replace(R.id.container, fragment, tag);

        frTransaction.addToBackStack(tag);

        frTransaction.commit();
    }
}
