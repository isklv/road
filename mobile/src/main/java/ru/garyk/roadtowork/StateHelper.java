package ru.garyk.roadtowork;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Sokolov on 22.05.2015.
 */
public class StateHelper {

    static private DatabaseHelper db;

    static private final String TABLE_NAME = "state";

    static public String getValue(Context context, String name){
        db = new DatabaseHelper(context);

        SQLiteDatabase sdb = db.getReadableDatabase();

        String result = null;

        String query = "SELECT value FROM "+TABLE_NAME+" WHERE name = ?";
        Cursor cursor = sdb.rawQuery(query, new String[]{name});

        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            result = cursor.getString(0);
        }
        db.close();
        return result;
    }

    static public void setValue(Context context, String name, String value){
        db = new DatabaseHelper(context);

        SQLiteDatabase sdb = db.getWritableDatabase();

        String query = "SELECT value FROM "+TABLE_NAME+" WHERE name = ?";
        Cursor cursor = sdb.rawQuery(query, new String[]{name});

        if(cursor.getCount() > 0){
            ContentValues values = new ContentValues();
            values.put("value", value);
            sdb.update(TABLE_NAME, values, "name" + "='" + name + "'", null);
        }else{
            ContentValues values = new ContentValues();
            values.put("value", value);
            values.put("name", name);
            sdb.insert(TABLE_NAME, null, values);
        }
        db.close();

    }

    public static void removeValue(Context context, String name){
        db = new DatabaseHelper(context);

        SQLiteDatabase sdb = db.getWritableDatabase();
        sdb.delete(TABLE_NAME, "name" + "='" + name +"'", null);
        db.close();
    }
}
