package ru.garyk.roadtowork;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sokolov on 22.05.2015.
 */
public class User implements RequestData.AsyncResponse{

    public static final Integer FB = 1;
    public static final Integer VK = 2;
    public static final Integer OK = 3;
    public static final String AUTH = "authorized";

    private final static String FNAME = "firstname";
    private final static String LNAME = "lastname";
    private final static String SID = "sid";
    private final static String TYPE = "type";
    private final static String AVATAR = "avatar";
    private final static String BIRTHDAY = "birthday";
    private final static String GENDER = "gender";


    private JSONObject resultJsonObject;
    private Context context;

    public User(Context context, String json, Integer type){

        this.context = context;

        try {

            JSONObject jsonObject = new JSONObject(json);
            resultJsonObject = new JSONObject();

            switch (type){
                case 1:

                    JSONObject resp = jsonObject;

                    resultJsonObject.put(FNAME, resp.getString("first_name"));
                    resultJsonObject.put(LNAME, resp.getString("last_name"));
                    resultJsonObject.put(SID, resp.getString("id"));
                    resultJsonObject.put(TYPE, "FB");
                    resultJsonObject.put(AVATAR, "http://graph.facebook.com/"+resp.getString("id")+"/picture");

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

                    Date birthday = null;
                    long time = 0;
                    try {
                        birthday = format.parse(resp.getString("birthday"));
                        time = birthday.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    resultJsonObject.put(BIRTHDAY, Long.toString(time));

                    String gender = "0";
                    if(resp.getString("gender") == "male"){
                        gender = "2";
                    }else if(resp.getString("gender") == "female"){
                        gender = "1";
                    }

                    resultJsonObject.put(GENDER, gender);

                    break;
                case 2:
                    JSONObject respVk = jsonObject.getJSONArray("response").getJSONObject(0);

                    resultJsonObject.put(FNAME, respVk.getString("first_name"));
                    resultJsonObject.put(LNAME, respVk.getString("last_name"));
                    resultJsonObject.put(SID, respVk.getString("id"));
                    resultJsonObject.put(TYPE, "VK");
                    resultJsonObject.put(AVATAR, respVk.getString("photo_400_orig"));

                    SimpleDateFormat formatVk = new SimpleDateFormat("dd.MM.yyyy");

                    Date birthdayVk = null;
                    long timeVk = 0;
                    try {
                        birthdayVk = formatVk.parse(respVk.getString("bdate"));
                        timeVk = birthdayVk.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    resultJsonObject.put(BIRTHDAY, Long.toString(timeVk));

                    resultJsonObject.put(GENDER, respVk.getString("sex"));

                    break;
                case 3:
                    JSONObject respOk = jsonObject;

                    resultJsonObject.put(FNAME, respOk.getString("first_name"));
                    resultJsonObject.put(LNAME, respOk.getString("last_name"));
                    resultJsonObject.put(SID, respOk.getString("uid"));
                    resultJsonObject.put(AVATAR, respOk.getString("pic320min"));
                    resultJsonObject.put(TYPE, "OK");

                    SimpleDateFormat formatOk = new SimpleDateFormat("yyyy-MM-dd");

                    Date birthdayOk = null;
                    long timeOk = 0;
                    try {
                        birthdayOk = formatOk.parse(respOk.getString("birthday"));
                        timeOk = birthdayOk.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    resultJsonObject.put(BIRTHDAY, Long.toString(timeOk));

                    String genderOk = "0";
                    if(respOk.getString("gender") == "male"){
                        genderOk = "2";
                    }else if(respOk.getString("gender") == "female"){
                        genderOk = "1";
                    }

                    resultJsonObject.put(GENDER, genderOk);
                    break;
            }


            new RequestData(this, RequestData.URL+"user", resultJsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void processFinish(String output){
        try {
            JSONObject result = new JSONObject(output);

            if(result.getInt("result") == 1){

                JSONArray names = resultJsonObject.names();

                for(int i = 0; i < names.length(); i++){
                    StateHelper.setValue(context, names.getString(i), resultJsonObject.getString(names.getString(i)));
                }

                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static boolean is_auth(Context context){
        if(StateHelper.getValue(context, SID) != null)
            return true;
        else
            return false;
    }
}
