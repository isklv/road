package ru.garyk.roadtowork;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.dialogs.VKCaptchaDialog;
import com.vk.sdk.util.VKUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    public static CallbackManager callbackManager;
    public static Odnoklassniki mOdnoklassniki;

    private static String sTokenKey = "Bpr2qOnWDvO5TQGYeLs7";
    private static String[] sMyScope = new String[]{VKScope.WALL, VKScope.NOHTTPS};

    private static final String OK_APP_ID = "1137846272";
    private static final String OK_APP_SECRET_KEY = "0A31D27D70F411938CFFB946";
    private static final String OK_APP_PUBLIC_KEY = "CBAJCDNEEBABABABA";


    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance(){
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mOdnoklassniki = Odnoklassniki.createInstance(getActivity().getApplicationContext(), OK_APP_ID, OK_APP_SECRET_KEY, OK_APP_PUBLIC_KEY);
        mOdnoklassniki.setTokenRequestListener(okListener);

        VKSdk.initialize(vkListener, getString(R.string.vk_app_id), VKAccessToken.tokenFromSharedPreferences(getActivity(), sTokenKey));

        FacebookSdk.sdkInitialize(getActivity());

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button vk = (Button) view.findViewById(R.id.vk_login);
        Button ok = (Button) view.findViewById(R.id.ok_login);
        LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));

        // If using in a fragment
        //loginButton.setFragment(this);
        // Other app specific specialization

        vk.setOnClickListener(loginVk);
        ok.setOnClickListener(loginOk);

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                new User(getActivity(), object.toString(), User.FB);
                                //{Response:  responseCode: 200, graphObject: {"last_name":"Sokolov","id":"10200414958772968","birthday":"08\/29\/1989","gender":"male","first_name":"Garik","email":"s0ber@inbox.ru","locale":"ru_RU"}, error: null}

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,locale");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        return view;
    }

    private View.OnClickListener loginVk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VKSdk.authorize(sMyScope, true, false);
        }
    };

    private View.OnClickListener loginOk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mOdnoklassniki.requestAuthorization(getActivity(), false, OkScope.VALUABLE_ACCESS);
        }
    };

    private VKSdkListener vkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(sMyScope);
            Log.v("APIVK", "Expired");
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            new AlertDialog.Builder(getActivity())
                    .setMessage(authorizationError.errorMessage)
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            newToken.saveTokenToSharedPreferences(getActivity(), sTokenKey);
            Intent i = new Intent(getActivity(), MainActivity.class);
            startActivity(i);
            Log.v("APIVK", "Receive");

            VKParameters params = new VKParameters();
            params.put(VKApiConst.USER_IDS, newToken.userId);
            params.put("fields", "sex, bdate, last_name, first_name, photo_400_orig, city, country");

            VKRequest request = VKApi.users().get(params);

            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    Log.v("APIVK", "Request");
                    Log.i("user", response.responseString);
                    new User(getActivity(), response.responseString, User.VK);
                    //{"response":[{"id":5440457,"first_name":"Игорь","sex":2,"last_name":"Соколов","photo_400_orig":"https:\/\/pp.vk.me\/c621117\/v621117457\/1d29e\/KEJvUw2wrH0.jpg","bdate":"29.8.1989","country":{"id":1,"title":"Россия"},"city":{"id":1469,"title":"Энгельс"}}]}
                }

                @Override
                public void onError(VKError error) {

                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {

                }
            });
            request.start();
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {

            //Intent i = new Intent(getActivity(), MainActivity.class);
            //startActivity(i);
            //Log.v("APIVK", "Accept");

        }
    };

    private OkTokenRequestListener okListener = new OkTokenRequestListener() {
        @Override
        public void onSuccess(String accessToken) {
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
            new LoadInfo().execute(getActivity());
        }

        @Override
        public void onError() {
            Log.v("APIOK", "Error");
            Toast.makeText(getActivity(), "Что-то пошло не так", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCancel() {
            Log.v("APIOK", "Cancel");
            Toast.makeText(getActivity(), "Что-то пошло не так", Toast.LENGTH_LONG).show();
        }
    };

    private class LoadInfo extends AsyncTask<Context, Void, String> {

        Context context;

        @Override
        protected String doInBackground(Context... context) {

            this.context = context[0];

            Map<String, String> requestParams = new HashMap<String, String>();
            requestParams.put("fields", "uid, locale, gender, birthday, last_name, first_name, pic320min, location");

            try {
                return mOdnoklassniki.request("users.getCurrentUser", requestParams, "get");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            new User(context, result, User.OK);

            //{"uid":"90524889426","birthday":"1989-08-29","locale":"ru","gender":"male","location":{"city":"Саратов","country":"RUSSIAN_FEDERATION","countryCode":"RU","countryName":"Россия"},"pic320min":"http://itd1.mycdn.me/image?id=771919735890&bid=771919735890&t=15&plc=API&viewToken=rE7jOJPFYpZDrQMjh6sqww&tkn=Dxy1hMBW79r4cvepcjOpFejkQps","first_name":"Игорь","last_name":"Соколов"}

        }

    }

    public static void createHash(Context context){
        // отпечаток сертификата
        String[] fingerprints = VKUtil.getCertificateFingerprint(context, context.getPackageName());
        for(int i = 0; i < fingerprints.length; i++){
            Log.i("fingerprints", fingerprints[i]);
        }
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign=Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }
}
